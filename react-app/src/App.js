import './App.css';
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Home from './pages/Home';
import Courses from './pages/Courses';

function App() {
  return (
    // Fragments are needed when there are two or more components, pages, or html elements present in the code
    <>
      {/* Self Closing Tags */}
      <AppNavBar /> 
      <Container>
        <Home />
        <Courses />
      </Container>
    </>
  );
}

export default App;
